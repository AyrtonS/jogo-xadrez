
public class Rei extends Pe�a {

	public Rei(String cor, int posicaoX, int posicaoY) {
		super("Rei", cor, posicaoX, posicaoY);
	}
	
	public boolean jogada(Pe�a tabuleiro[][], int posicaoX, int posicaoY) {
		int x, y, i, j;
		if(tabuleiro[posicaoX][posicaoY] != null)
    	{
    		if((getCor().equals("branco") && tabuleiro[posicaoX][posicaoY].getCor().equals("branco")) || 
    			(getCor().equals("preto") && tabuleiro[posicaoX][posicaoY].getCor().equals("preto")))// retorna flaso se a posi��o de destino tiver uma pe�a da mesma cor
    		{
    			return false;
    		}
    	}
		x = getPosicaoX();
		y = getPosicaoY();
		if(x == posicaoX && ((y + 1) == posicaoY || (y - 1) == posicaoY))
		{
			for(i = 0; i < 8; i++)
			{
				for(j = 0; j < 8; j++)
				{
					if(tabuleiro[i][j]!=null)
					{	
						if(tabuleiro[i][j].getCor().equals(getCor()) == false)// verifica se rei ficara em Xeque
						{
							if(tabuleiro[i][j].jogada(tabuleiro,posicaoX, posicaoY))
							{
								return false;
							}
						}
					}
				}
			}
			setPosicaoX(posicaoX);
			setPosicaoY(posicaoY);
			return true;
		}
		else if(y == posicaoY && ((x + 1) == posicaoX || (x - 1) == posicaoX))
		{
			for(i = 0; i < 8; i++)
			{
				for(j = 0; j < 8; j++)
				{
					if(tabuleiro[i][j]!=null)
					{	
						if(tabuleiro[i][j].getCor().equals(getCor()) == false)// verifica se rei ficara em Xeque
						{
							if(tabuleiro[i][j].jogada(tabuleiro,posicaoX, posicaoY))
							{
								return false;
							}
						}
					}
				}
			}
			setPosicaoX(posicaoX);
			setPosicaoY(posicaoY);
			return true;
		}
		else if((y - 1) == posicaoY && ((x + 1) == posicaoX || (x - 1) == posicaoX))
		{
			for(i = 0; i < 8; i++)
			{
				for(j = 0; j < 8; j++)
				{
					if(tabuleiro[i][j]!=null)
					{	
						if(tabuleiro[i][j].getCor().equals(getCor()) == false)// verifica se rei ficara em Xeque
						{
							if(tabuleiro[i][j].jogada(tabuleiro,posicaoX, posicaoY))
							{
								return false;
							}
						}
					}
				}
			}
			setPosicaoX(posicaoX);
			setPosicaoY(posicaoY);
			return true;
		}
		else if((y + 1) == posicaoY && ((x + 1) == posicaoX || (x - 1) == posicaoX))
		{
			for(i = 0; i < 8; i++)
			{
				for(j = 0; j < 8; j++)
				{
					if(tabuleiro[i][j]!=null)
					{	
						if(tabuleiro[i][j].getCor().equals(getCor()) == false)// verifica se rei ficara em Xeque
						{
							if(tabuleiro[i][j].jogada(tabuleiro,posicaoX, posicaoY))
							{
								return false;
							}
						}
					}
				}
			}
			setPosicaoX(posicaoX);
			setPosicaoY(posicaoY);
			return true;
		}
		return false;
	}
}
