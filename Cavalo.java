
public class Cavalo extends Pe�a {

	public Cavalo(String cor, int posicaoX, int posicaoY) {
		super("Cavalo", cor, posicaoX, posicaoY);
	}
	
    public boolean jogada(Pe�a tabuleiro[][], int posicaoX, int posicaoY)
    {
    	if(tabuleiro[posicaoX][posicaoY] != null)
    	{
    		if((getCor().equals("branco") && tabuleiro[posicaoX][posicaoY].getCor().equals("branco")) || 
    			(getCor().equals("preto") && tabuleiro[posicaoX][posicaoY].getCor().equals("preto")))// retorna flaso se a posi��o de destino tiver uma pe�a da mesma cor
    		{
    			return false;
    		}
    	}
    	if(((getPosicaoX() + 2) == posicaoX) && ((getPosicaoY() + 1) == posicaoY || (getPosicaoY() - 1) == posicaoY))
    	{
    		setPosicaoX(posicaoX);
    		setPosicaoY(posicaoY);
    		return true;
    	}
    	if(((getPosicaoX() - 2) == posicaoX) && ((getPosicaoY() + 1) == posicaoY || (getPosicaoY() - 1) == posicaoY))
    	{
    		setPosicaoX(posicaoX);
    		setPosicaoY(posicaoY);
    		return true;
    	}
    	if(((getPosicaoY() - 2) == posicaoY) && ((getPosicaoX() + 1) == posicaoX || (getPosicaoX() - 1) == posicaoX))
    	{
    		setPosicaoX(posicaoX);
    		setPosicaoY(posicaoY);
    		return true;
    	}
    	if(((getPosicaoY() + 2) == posicaoY) && ((getPosicaoX() + 1) == posicaoX || (getPosicaoX() - 1) == posicaoX))
    	{
    		setPosicaoX(posicaoX);
    		setPosicaoY(posicaoY);
    		return true;
    	}
    	return false;
    }
}
