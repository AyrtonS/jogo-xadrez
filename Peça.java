
abstract class Pe�a {
	private String nome;
	private String cor;
	private int posicaoX;
	private int posicaoY;
		
	public Pe�a(String nome, String cor, int posicaoX, int posicaoY) {
		this.nome = nome;
		this.cor = cor;
		this.posicaoX = posicaoX;
		this.posicaoY = posicaoY;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCor() {
		return cor;
	}
	
	public int getPosicaoX() {
		return posicaoX;
	}

	public void setPosicaoX(int posicaoX) {
		this.posicaoX = posicaoX;
	}

	public int getPosicaoY() {
		return posicaoY;
	}

	public void setPosicaoY(int posicaoY) {
		this.posicaoY = posicaoY;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	protected abstract boolean jogada(Pe�a[][] tabuleiro, int posicaoX2, int posicaoY2);
	
}
