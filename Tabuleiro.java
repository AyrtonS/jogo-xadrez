
public class Tabuleiro {
 public Pe�a tabuleiro[][] = new Pe�a[8][8];
 
 public Tabuleiro()
 {
	 tabuleiro[0][0] = new Torre("branco",0,0);
	 tabuleiro[0][1] = new Cavalo("branco", 0,1);
	 tabuleiro[0][2] = new Bispo("branco",0,2);
	 tabuleiro[0][3] = new Rainha("branco",0,3);
	 tabuleiro[0][4] = new Rei("branco",0,4);
	 tabuleiro[0][5] = new Bispo("branco",0,5);
	 tabuleiro[0][6] = new Cavalo("branco",0,6);
	 tabuleiro[0][7] = new Torre("branco",0,7);
	 tabuleiro[1][0] = new Pe�o("branco",1,0);
	 tabuleiro[1][1] = new Pe�o("branco",1,1);
	 tabuleiro[1][2] = new Pe�o("branco",1,2);
	 tabuleiro[1][3] = new Pe�o("branco",1,3);
	 tabuleiro[1][4] = new Pe�o("branco",1,4);
	 tabuleiro[1][5] = new Pe�o("branco",1,5);
	 tabuleiro[1][6] = new Pe�o("branco",1,6);
	 tabuleiro[1][7] = new Pe�o("branco",1,7);
	 
	 tabuleiro[7][0] = new Torre("preto",7,0);
	 tabuleiro[7][1] = new Cavalo("preto",7,1);
	 tabuleiro[7][2] = new Bispo("preto",7,2);
	 tabuleiro[7][3] = new Rainha("preto",7,3);
	 tabuleiro[7][4] = new Rei("preto",7,4);
	 tabuleiro[7][5] = new Bispo("preto",7,5);
	 tabuleiro[7][6] = new Cavalo("preto",7,6);
	 tabuleiro[7][7] = new Torre("preto",7,7);
	 tabuleiro[6][0] = new Pe�o("preto",6,0);
	 tabuleiro[6][1] = new Pe�o("preto",6,1);
	 tabuleiro[6][2] = new Pe�o("preto",6,2);
	 tabuleiro[6][3] = new Pe�o("preto",6,3);
	 tabuleiro[6][4] = new Pe�o("preto",6,4);
	 tabuleiro[6][5] = new Pe�o("preto",6,5);
	 tabuleiro[6][6] = new Pe�o("preto",6,6);
	 tabuleiro[6][7] = new Pe�o("preto",6,7);
 }
 
 public void jogada(int posicaoX, int posicaoY, int posicaoFX, int posicaoFY)
 {
	if(tabuleiro[posicaoX][posicaoY] != null)
	{
		if(tabuleiro[posicaoX][posicaoY].jogada(tabuleiro,posicaoFX,posicaoFY))
		{
			tabuleiro[posicaoFX][posicaoFY] = tabuleiro[posicaoX][posicaoY];
			tabuleiro[posicaoX][posicaoY] = null;
		}
	}
 }
}
