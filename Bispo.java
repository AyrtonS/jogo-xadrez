
public class Bispo extends Pe�a {

	public Bispo(String cor, int posicaoX, int posicaoY) {
		super("Bispo", cor, posicaoX, posicaoY);
	}

	public boolean jogada(Pe�a tabuleiro[][], int posicaoX, int posicaoY) {
		int x, y;
		if(tabuleiro[posicaoX][posicaoY] != null)
    	{
    		if((getCor().equals("branco") && tabuleiro[posicaoX][posicaoY].getCor().equals("branco")) || 
    			(getCor().equals("preto") && tabuleiro[posicaoX][posicaoY].getCor().equals("preto")))// retorna flaso se a posi��o de destino tiver uma pe�a da mesma cor
    		{
    			return false;
    		}
    	}
		x = getPosicaoX();
		y = getPosicaoY();
		if(x > posicaoX && y > posicaoY)
		{
			do
			{
				if(tabuleiro[x][y] != null && x != getPosicaoX())
				{
					return false;
				}
				x--;
				y--;
			}while((x != posicaoX && y != posicaoY) && (x > 0 && y >  0));
			if(x == posicaoX && y == posicaoY)
			{
				setPosicaoX(x);
				setPosicaoY(y);
				return true;
			}
		}
		else if(x > posicaoX && y < posicaoY)
		{
			do
			{
				if(tabuleiro[x][y] != null && x != getPosicaoX())
				{
					return false;
				}
				x--;
				y++;
			}while((x != posicaoX && y != posicaoY) && (x > 0 && y < 7));
			if(x == posicaoX && y == posicaoY)
			{
				setPosicaoX(x);
				setPosicaoY(y);
				return true;
			}
		}
		else if(x < posicaoX && y > posicaoY)
		{
			do
			{
				if(tabuleiro[x][y] != null && x != getPosicaoX())
				{
					return false;
				}
				x++;
				y--;
			}while((x != posicaoX && y != posicaoY) && (x < 7 && y > 0));
			if(x == posicaoX && y == posicaoY)
			{
				setPosicaoX(x);
				setPosicaoY(y);
				return true;
			}
		}
		else if(x < posicaoX && y < posicaoY)
		{
			do
			{
				if(tabuleiro[x][y] != null && x != getPosicaoX())
				{
					return false;
				}
				x++;
				y++;
			}while((x != posicaoX && y != posicaoY) && (x < 7 && y < 7));
			if(x == posicaoX && y == posicaoY)
			{
				setPosicaoX(x);
				setPosicaoY(y);
				return true;
			}
		}
		return false;
	}
}
