
public class Pe�o extends Pe�a{

	public Pe�o(String cor, int posicaoX, int posicaoY) {
		super("Pe�o", cor, posicaoX, posicaoY);
		
	}

	public boolean jogada(Pe�a tabuleiro[][], int posicaoX, int posicaoY){
		
		if(getCor().equals("branco"))
		{
			if(tabuleiro[posicaoX][posicaoY] == null)
			{
				if(((getPosicaoX() + 1) == posicaoX) && (posicaoY == getPosicaoY()))
				{
					setPosicaoX(posicaoX);
					setPosicaoY(posicaoY);
					return true;
				}
			}
			else if(tabuleiro[posicaoX][posicaoY].getCor().equals("preto"))
			{
				if(((getPosicaoX() + 1) == posicaoX) && ((getPosicaoY() - 1) == posicaoY || (getPosicaoY() + 1) == posicaoY))
				{
					setPosicaoX(posicaoX);
					setPosicaoY(posicaoY);
					return true;
				}
			}
		}
		
		if(getCor().equals("preto"))
		{
			if(tabuleiro[posicaoX][posicaoY] == null)
			{
				if(((getPosicaoX() - 1) == posicaoX) && (posicaoY == getPosicaoY()))
				{
					setPosicaoX(posicaoX);
					setPosicaoY(posicaoY);
					return true;
				}
			}
			else if(tabuleiro[posicaoX][posicaoY].getCor().equals("branco"))
			{
				if(((getPosicaoX() - 1) == posicaoX) && ((getPosicaoY() - 1) == posicaoY || (getPosicaoY() + 1) == posicaoY))
				{
					setPosicaoX(posicaoX);
					setPosicaoY(posicaoY);
					return true;
				}
			}
		}
		
		return false;
	}
}
