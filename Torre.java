
public class Torre  extends Pe�a{

	public Torre(String cor, int posicaoX, int posicaoY) {
		super("Torre", cor, posicaoX, posicaoY);
	}
	
	public boolean jogada(Pe�a tabuleiro[][], int posicaoX, int posicaoY) {
		int x, y;
		if(tabuleiro[posicaoX][posicaoY] != null)
    	{
    		if((getCor().equals("branco") && tabuleiro[posicaoX][posicaoY].getCor().equals("branco")) || 
    			(getCor().equals("preto") && tabuleiro[posicaoX][posicaoY].getCor().equals("preto")))// retorna flaso se a posi��o de destino tiver uma pe�a da mesma cor
    		{
    			return false;
    		}
    	}
		if(getPosicaoX() == posicaoX)// movimento em y
		{
			x = getPosicaoY();//armazena posicao da pessa para nao chamar muitas vezes a fun��o
			y = posicaoY;// posi��o final da pe�a em y
			if(y > x)// verifica se a posi��o da pe�a e menor que a final
			{
				do//verifica se o caminha at� a posi��o da pe�a esta livre
				{
					if(tabuleiro[posicaoX][y] != null && y != posicaoY)
					{
						return false;
					}
					y--;
				}while(y > x);
			}
			else if(y < x)
			{
				do//verifica se o caminha at� a posi��o da pe�a esta livre
				{
					if(tabuleiro[posicaoX][y] != null && y != posicaoY)
					{
						return false;
					}
					y++;
				}while(y < x);
			}
		}
		else if(getPosicaoY() == posicaoY)// movimento em x
		{
			y = getPosicaoX();//armazena posicao da pe�a para nao chamar muitas vezes a fun��o
			x = posicaoX;// posi��o final da pe�a em x
			if(y < x)// verifica se a posi��o da pe�a e menor que a final
			{
				do//verifica se o caminha at� a posi��o da pe�a esta livre
				{
					if(tabuleiro[x][posicaoY] != null && x != posicaoX)
					{
						return false;
					}
					x--;
				}while(y < x);
			}
			else if(y > x)
			{
				do//verifica se o caminha at� a posi��o da pe�a esta livre
				{
					if(tabuleiro[x][posicaoY] != null && x != posicaoX)
					{
						return false;
					}
					x++;
				}while(y > x);
			}
		}
		else
		{
			return false;
		}
		setPosicaoX(posicaoX);
		setPosicaoY(posicaoY);
		return true;
	}

}
